# anime.pl

Helper for easy watching anime (or tv series) with (or with out) external
subtitles.


## Options
~~~
    -v glob pattern for video
    -s glob pattern for subtitles
    -n episode number (pattern)
    -l play last watched episode
    -d print default directory
    -D remember current directory as default
    -C clean info about current directory
    -i print info about current directory
    -I print info about all directories
    -h show this help
~~~


## Usage
```
anime.pl -v 'qq["* $n *"]' -s 'qq[sub/* $n *]' -n 01
```
Store patterns to db and play first episode.

```
anime.pl
```
Play second episode.

```
anime.pl -l
```
Play last watched episode (second).

### .zshrc

```
cdanime='cd "$(anime.pl -d)"'
```

