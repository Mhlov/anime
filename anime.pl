#!/usr/bin/perl
#===============================================================================
#  DESCRIPTION: Console helper for watching anime with subtitles.
#
#         TODO: 
#
#       AUTHOR: ΜΗΛΟΝ
#      CREATED: 14-02-2022 08:32
#      LICENSE: Artistic License 1.0
#===============================================================================
package Anime;
use Modern::Perl '2020';
use utf8;
use strict;
use autodie;
use warnings;
use English;

# Unicode
use warnings  qw/FATAL utf8/;
use open      qw/:std :utf8/;
use charnames qw/:full/;
use feature   qw/unicode_strings/;

use Term::ANSIColor qw(:constants);  # https://perldoc.perl.org/Term::ANSIColor

use Getopt::Long qw/:config posix_default gnu_getopt no_ignore_case/;

# DEBUGGING
#use diagnostics;
use Carp;                       # 'carp' for warn, 'croak' for die
                                # https://perldoc.perl.org/Carp.html

use Data::Printer;              # Usage: p @array;
# or use DDP;                   # https://metacpan.org/pod/Data::Printer

use Cwd qw/abs_path getcwd/;

my $version = '0.3';

my %option = (
    vid_pattern         => undef,
    sub_pattern         => undef,
    episode             => undef,
    sub_id              => undef,
    get_info            => 0,
    get_all_info        => 0,
    get_last_dir        => 0,
    clean_db            => 0,
    clean_dir_info      => 0,
    play_last_episode   => 0,
    downscale           => 0,
    help                => 0,
    downscale_width     => 320,
    downscale_height    => -1,
    downscale_out_dir   => "downscaled",
);

my $config_file = "$ENV{HOME}/.config/anime.yml";


sub help {
    print <<'_EOH_';
anime.pl - Helper for easy watching anime (or tv series) with (or with out)
external subtitles.
OPTIONS:
    -v <pattern> glob pattern for video
    -s <pattern> glob pattern for subtitles
    -S <id|auto|no> select the subtitle stream specified by id;
       auto - select the default; no - disable subtitles
    -n <pattern> episode number (pattern)
    -l play last watched episode
    -d print last directory
    -D clean info about current directory
    -C clean info about non-existent directories
    -i print info about current directory
    -I print info about all directories
    --downscale - downscale all video files in current dir
    -h show this help
USAGE:
    anime.pl -v 'qq["* $n *"]' -s 'qq[sub/*_${n}_*]' -n 01
        store the patterns to db and play first episode
    anime.pl
        play second episode
    anime.pl -l
        play last watched episode (second)
_EOH_
}

sub options {
    GetOptions(
        "pattern|v=s"     => \$option{vid_pattern},
        "sub_pattern|s=s" => \$option{sub_pattern},
        "sid|S=s"         => \$option{sub_id},
        "episode|n=s"     => \$option{episode},
        "info|i"          => \$option{get_info},
        "all|I"           => \$option{get_all_info},
        "get_dir|d"       => \$option{get_last_dir},
        "clean_dir|D"     => \$option{clean_dir_info},
        "clean|C"         => \$option{clean_db},
        "last|l"          => \$option{play_last_episode},
        "downscale"       => \$option{downscale},
        "help|h"          => \$option{help},
    ) or die ">_<\n";
}

sub downscale_current_dir {
    opendir my $dh, '.' or die "Can't open current dir: $!";
    unless (-e $option{downscale_out_dir}) {
        mkdir $option{downscale_out_dir}
            or die "Can't create '$option{downscale_out_dir} dir: $!";
    }

    while (my $file = readdir $dh) {
        if (-f $file && $file =~ /^(.+)\.(mkv|avi|mp4|mpg|mpeg)$/i) {

            my $out_file = "downscaled/$1.ds.$2";

            if (-e $out_file) {
                warn BRIGHT_YELLOW,
                    "[Skip] File '$out_file' already exists.",
                    RESET, "\n";
                next;
            }

            print STDERR CYAN, "Downscaling '$file' => '$out_file'...";

            my $vf = "scale=$option{downscale_width}:$option{downscale_height}";
            $vf .= ',pad=ceil(iw/2)*2:ceil(ih/2)*2';

            system(ffmpeg => qw/-loglevel warning/,
                '-i', $file,
                '-vf', $vf,
                '-map', '0',
                '-c:s', 'mov_text',
                #'-b:v', '25k',
                $out_file,
            ) and die RED, "\n[ Error ] Somesing went wrong!", RESET, "\n";

            warn GREEN, "\b"x3, " [ OK ]", RESET, "\n";
        }
    }

    closedir $dh;
}

sub video_pattern {
    my ($db) = @_;
    my $vid_pattern;

    # vid_pattern from options
    if (defined $option{vid_pattern}) {
        $vid_pattern = $option{vid_pattern};
    }
    # or from db
    elsif ($db->has_vid_pattern) {
        $vid_pattern = $db->get_vid_pattern;
    }
    else {
        die "Video pattern is not specified.\n";
    }

    return $vid_pattern;
}

sub subtitles_pattern {
    my ($db) = @_;
    my $sub_pattern;

    if (defined $option{sub_pattern}) {
        $sub_pattern = $option{sub_pattern};
    }
    elsif ($db->has_sub_pattern) {
        $sub_pattern = $db->get_sub_pattern;
    }

    return $sub_pattern;
}

sub sub_id {
    my ($db) = @_;
    my $sub_id;

    if (defined $option{sub_id}) {
        $sub_id = $option{sub_id};
    }
    elsif ($db->has_sub_id) {
        $sub_id = $db->get_sub_id;
    }

    return $sub_id;
}

sub push_info_to_db {
    my ($db) = @_;
    foreach my $k (qw/vid_pattern sub_pattern episode sub_id/) {
        next if not defined $option{$k};
        $db->set_key($k, $option{$k});
    }
}

sub print_last_dir {
    my ($db) = @_;
    my $ldir =  $db->get_last_dir;
    if (defined $ldir) {
        print $ldir;
        return 0;
    } else {
        warn "Last anime directoy is not specified.\n";
        return 1;
    }
}

sub print_info {
    my ($db, $all) = @_;
    my @dirs = $all ? $db->get_all_dirs : $db->get_selected_dir;

    foreach my $dir (@dirs) {
        say GREEN, $dir, RESET;
        foreach my $k ( sort keys %{ $db->{db}{$dir} } ) {
            say "    $k: ", $db->{db}{$dir}{$k};
        }
    }
}

sub print_play_info {
    my $inf = shift;
    say
        MAGENTA, "Video file: ", RESET,
        $inf->get_video_files, "\n",
        map { CYAN."Sub file: ".RESET . "$_\n" } $inf->get_subtitles;
}

sub play {
    my $inf = shift;
    exec mpv => '--fs',
        $inf->get_video_files(),
        (map { "--sub-file=$_" } $inf->get_subtitles),
        (map { "--sid=$_" } $inf->get_sub_id // () );
}


sub main {
    options;

    help and return 0 if $option{help};

    my $inf = Anime::Info->new(
        dir => getcwd(),
        opt => \%option,
    );

    my $db = Anime::DB->new($config_file);

    $db->select_dir($inf->dir);

    if ($option{downscale}) {
        downscale_current_dir();
        return 0;
    }

    if ($option{get_last_dir}) {
        return print_last_dir($db);
    }

    if ($option{clean_db}) {
        $db->remove_non_existent_dirs;
        $db->save;
        return 0;
    }

    if ($option{clean_dir_info}) {
        $db->delete_dir_info;
        $db->save;
        return 0;
    }

    if ($option{get_info}) {
        print_info($db);
        return 0;
    }

    if ($option{get_all_info}) {
        print_info($db, 1);
        return 0;
    }


    # trying to get the episode number from options or db
    $inf->{episode} = $option{episode} ? $option{episode}
                                       : $db->get_episode;

    # trying to get the video pattern
    $inf->{vid_pattern} = video_pattern($db);

    die "The glob video pattern is wrong\n"
        unless $inf->vid_pattern_is_ok;

    # trying to get the subtitles pattern
    $inf->{sub_pattern} = subtitles_pattern($db);

    # trying to get the subtitle stream id
    $inf->{sub_id} = sub_id($db);

    # insert the glob patterns and the episode to the db
    push_info_to_db($db);

    # if the episode not defined
    if (not defined $option{episode}) {

        # play last episode
        if ($option{play_last_episode}) {
            if ($db->has_episode) {
                $inf->{episode} = $db->get_episode;
            } else {
                die "Last episode info not found\n";
            }
        }

        # predict next episode
        elsif ($inf->{episode} =~ /^\d+$/) {
            if ($inf->predict_next_episode) {
                $db->set_episode($inf->{episode});
            }
            else {
                die (
                    "The wrong prediction for the next episode",
                    " ($inf->{episode}). File not found.\n"
                );
            }
        }

        else {
            die "Cant predict the next episode :(\n";
        }
    }

    $db->set_last_dir($inf->dir);
    $db->save;

    print_play_info($inf);
    play($inf);

}

exit main(@ARGV);

################################################################################
package Anime::Info {
    use strict;
    use warnings;
    use Carp;
    use Data::Printer;

    sub new {
        my ($class, %args) = @_;
        my $self = bless {
            opt => {},
            dir => undef,
            vid_pattern => undef,
            sub_pattern => undef,
            sub_id      => undef,
            %args,
        }, $class;

        return $self;
    }

    sub dir {
        return shift->{dir};
    }

    sub get_video_files {
        my $self = shift;
        my $n = shift // $self->{episode};
        return glob(eval($self->{vid_pattern}));
    }

    sub get_subtitles {
        my $self = shift;
        my $n = $self->{episode};
        return () if not defined  $self->{sub_pattern};
        return glob(eval($self->{sub_pattern}));
    }

    sub get_sub_id {
        my $self = shift;
        return $self->{sub_id};
    }

    sub vid_pattern_is_ok {
        my $self = shift;
        my $n = $self->{episode};
        if ( ($self->get_video_files)[0] ) {
            return 1;
        }
        else {
            return 0;
        }
    }

    sub predict_next_episode {
        my $self = shift;
        my $n = $self->{episode};
        my $len = length $n;

        $n = sprintf "%0${len}d", $n + 1;

        if ( -e ($self->get_video_files($n))[0] ) {
            $self->{episode} = $n;
            return $n;
        } else {
            return undef;
        }
    }

    1;
}

################################################################################
package Anime::DB {
    use strict;
    use warnings;
    use Carp;
    use Data::Printer;
    use YAML::XS;

    sub _read_yaml {
        my $file = shift;
        my $yaml;
        $yaml = YAML::XS::LoadFile($file) if -e $file;
        return $yaml;
    }

    sub _write_yaml {
        my ($file, $data) = @_;
        YAML::XS::DumpFile($file, $data);
    }

    sub _read_db {
        my $self = shift;
        my $yaml = _read_yaml($self->{_db_file});

        foreach my $key (keys %$yaml) {
            next if $key =~ /^_/;
            $self->{$key} = $yaml->{$key};
        }
    }

    sub _write_db {
        my $self = shift;
        my $yaml = _read_yaml($self->{_db_file}) // {};

        foreach my $key (keys %$self) {
            next if $key =~ /^_/;
            $yaml->{$key} = $self->{$key};
        }

        _write_yaml($self->{_db_file}, $yaml);
    }

    sub new {
        my ($class, $file) = @_;
        my $self = bless {
            _db_file => $file,
            _has_changes => 0,
            _dir => undef,
            db => {},
        }, $class;

        $self->_read_db;

        return $self;
    }

    sub save {
        my $self = shift;
        $self->_write_db if $self->{_has_changes};
        $self->{_has_changes} = 0;
        return $self;
    }

    sub get_last_dir {
        my $self = shift;
        return $self->{anime_dir};
    }

    sub set_last_dir {
        my $self = shift;
        my $new_dir = shift;
        if (!defined $self->{anime_dir} or $new_dir ne $self->{anime_dir}) {
            $self->{anime_dir} = $new_dir;
            $self->{_has_changes} = 1;
        }
    }

    sub select_dir {
        my ($self, $dir) = @_;
        $self->{_dir} = $dir;
    }

    sub get_selected_dir {
        return shift->{_dir};
    }

    sub get_all_dirs {
        return sort keys %{ shift->{db} };
    }

    sub remove_non_existent_dirs {
        my $self = shift;
        my @dirs = $self->get_all_dirs();
        while (my $dir = shift @dirs) {
            $self->delete_dir_info($dir) unless -e $dir;
        }
    }

    sub delete_dir_info {
        my $self = shift;
        my $dir = shift // $self->{_dir};
        if (exists $self->{db}->{$dir}) {
            delete $self->{db}->{$dir};
            $self->{_has_changes} = 1;
        }
        if (exists $self->{anime_dir} and $self->{anime_dir} eq $dir) {
            $self->{anime_dir} = undef;
            $self->{_has_changes} = 1;
        }
    }

    sub has_key {
        my ($self, $key) = @_;
        my $dir = $self->{_dir};

        if (exists $self->{db}{$dir}{$key}) {
            return 1;
        } else {
            return 0;
        }
    }

    sub get_key {
        my ($self, $key) = @_;
        my $dir = $self->{_dir};

        if (exists $self->{db}{$dir}{$key}) {
            return $self->{db}{$dir}{$key};
        } else {
            return undef;
        }
    }

    sub set_key {
        my ($self, $key, $value) = @_;
        my $dir = $self->{_dir};

        if( !defined $key or !defined $dir) {
            return undef;
        }

        if ( not exists $self->{db}{$dir}{$key}
             or $self->{db}{$dir}{$key} ne $value )
        {
            $self->{db}{$dir}{$key} = $value;
            $self->{_has_changes} = 1;
        }
    }

    sub AUTOLOAD {
        my ($type, $name) = our $AUTOLOAD =~ /::(has|get|set)_([\w_]+)$/;

        if ($type and $name) {
            return shift->has_key($name, @_) if $type eq 'has';
            return shift->get_key($name, @_) if $type eq 'get';
            return shift->set_key($name, @_) if $type eq 'set';
        }
        else {
            croak ("AUTOLOAD: Undefined method $AUTOLOAD.");
        }
    }

    sub DESTROY {}


    1;
}
