CURRENT_DIR=$(shell pwd -P)
NAME=anime
BIN_NAME=anime.pl
TARGET_BIN_DIR=${HOME}/bin

TARGET_BIN=${TARGET_BIN_DIR}/${BIN_NAME}

ORIGIN_BIN=${CURRENT_DIR}/${BIN_NAME}

help:
	@echo "Usage:"
	@echo "\tmake install | link | uninstall"


install:
	@test -e "${TARGET_BIN}" || \
		cp "${ORIGIN_BIN}" "${TARGET_BIN}"

link:
	@test -e "${TARGET_BIN}" || \
		ln -s "${ORIGIN_BIN}" "${TARGET_BIN}"

uninstall:
	@test -e "${TARGET_BIN}" && \
		rm "${TARGET_BIN}"

